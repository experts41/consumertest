package com.cas.cdc.contract;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.json.AutoConfigureJsonTesters;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.testcontainers.containers.GenericContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;
import org.springframework.web.client.RestTemplate;
import com.cas.cdc.OrderServiceConsumer;
import wiremock.com.fasterxml.jackson.core.JsonProcessingException;
import wiremock.com.fasterxml.jackson.databind.ObjectMapper;
import wiremock.com.fasterxml.jackson.databind.SerializationFeature;

import static org.assertj.core.api.Assertions.assertThat;
import static java.util.Objects.requireNonNull;

@Testcontainers
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc

public class ContractConsumerTest {

    @Value("${service-provider.url}")
    private String baseUrl;
    @Container
    private static final GenericContainer<?> app = new GenericContainer<>("maven:3.8.5-openjdk-17")
            .withExposedPorts(8080);

    @DynamicPropertySource
    static void configure(DynamicPropertyRegistry registry) {
        registry.add("service-provider.url", () -> "http://" + app.getContainerIpAddress() + ":" + app.getMappedPort(8080));
    }

    @Test
    void test() throws JsonProcessingException {
        RestTemplate restTemplate = new RestTemplate();

        ResponseEntity<OrderServiceConsumer.Order[]> response = restTemplate.
                getForEntity(baseUrl + "/assets",
                        OrderServiceConsumer.Order[].class);






        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);

        OrderServiceConsumer.Order[] orders = response.getBody();

        ObjectMapper objectMapper = new ObjectMapper().enable(SerializationFeature.INDENT_OUTPUT);
        String json = objectMapper.writeValueAsString(orders);
        System.out.println(json);
        assertThat(orders).isNotNull();
        assertThat(orders).hasSizeGreaterThan(0);

        OrderServiceConsumer.Order order = orders[0]; // Assuming there is at least one order
        assertThat(requireNonNull(order.getAssetNumber())).isEqualTo("1000002116153236");
        assertThat(requireNonNull(order.getAvailableBalance())).isEqualTo(2000);
        assertThat(requireNonNull(order.getExpirationDate())).isEqualTo("2040-12-31T23:59:59.999-06:00");
        assertThat(requireNonNull(order.getId())).isEqualTo("dc582591-4b05-4ca4-9bb0-4d9dab56581d");
        assertThat(requireNonNull(order.getStatus())).isEqualTo("OPEN");
        assertThat(requireNonNull(order.getType())).isEqualTo("RTF");
        assertThat(requireNonNull(order.getUom())).isEqualTo("USD");
    }
}
