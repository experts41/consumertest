package com.cas.cdc;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;

import static java.util.Objects.isNull;
import static org.springframework.http.ResponseEntity.notFound;
import static org.springframework.http.ResponseEntity.ok;

@RestController
public class OrderServiceConsumer {

    private static final String SERVICE_PROVIDER = "http://localhost:8080";
    private final RestTemplate restTemplate;

    public OrderServiceConsumer(RestTemplateBuilder restTemplateBuilder) {
        this.restTemplate = restTemplateBuilder.build();
    }

    @GetMapping(value = "/assets", produces = "application/json")
    public ResponseEntity<Order> getAssets() {
        try {
            ResponseEntity<Order[]> responseEntity = this.restTemplate.getForEntity(SERVICE_PROVIDER + "/assets", Order[].class);
            Order[] orders = responseEntity.getBody();
            if (isNull(orders) || orders.length == 0) {
                return notFound().build();
            }

            return ok((Order) Arrays.asList(orders));
        } catch (Exception e) {
            return notFound().build();
        }

    }

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    public static class Order {
        private String assetNumber;
        private double availableBalance;
        private String expirationDate;
        private String id;
        private String status;
        private String type;
        private String uom;
    }
}
